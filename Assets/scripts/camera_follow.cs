﻿using UnityEngine;
using System.Collections;
using System; 

public class camera_follow : MonoBehaviour
{

	private int equipa, jogador;

	private float mousex, mousey;
	private float alpha;
	private float beta;
	//cores para texto agentes
	private Color[] cores = { Color.blue, Color.red, Color.green, Color.magenta};

	private void info_player ()
	{
		//referencia jogador
		Jogador player = Jogo.equipas [this.equipa].get_jogadores () [this.jogador];
		//stamina
		//float stamina = player.get_current_stamina ();
		//pontos
		//int pontos = player.get_pontos ();
		//peso
		//float peso = player.get_peso ();
		GUIText texto = GameObject.Find ("info_player").GetComponent<GUIText> ();

		texto.text = player.get_gui_text ();



		/*texto.text = "Player: " + this.jogador + "\n  " +
			"Points: " + pontos + 
			"\n" +
			"Stamina: " + Math.Round (stamina, 3) + "\n" +
			"Weight: " + peso + "\n" +
			"immovable: "+ player.get_immovable()+"\n" +
			"ultima acçao: "+player.get_utlima_accao() + "\n" +
			"velocudade atual: "+player.get_current_speed();*/
		texto.color = cores [this.equipa];
	}

	private void position_camera ()
	{
		float newmousex = Input.mousePosition.x;
		float newmousey = Input.mousePosition.y;

		Jogador j = Jogo.equipas [this.equipa].get_jogadores () [this.jogador];

		this.gameObject.GetComponent<Camera> ().transform.position = new Vector3 (
				(5f * Mathf.Sin (alpha)) + j.get_objeto ().transform.position.x,
				(2f + Mathf.Sin (beta)) + j.get_objeto ().transform.position.y,
			    (5f * Mathf.Cos (alpha)) + j.get_objeto ().transform.position.z
		);

		transform.LookAt (j.get_objeto ().transform);

		float easefactor = 10f;

		if (newmousex != this.mousex) {
			float cameraRotationHor = (newmousex - this.mousex) * easefactor * Time.deltaTime;
			float cameraRotatioVert = (newmousey - this.mousey) * easefactor * Time.deltaTime;

			this.alpha += cameraRotationHor;

			this.beta += cameraRotatioVert;
		}

		this.mousex = newmousex;
		this.mousey = newmousey;

	}

	// Use this for initialization
	void Start ()
	{
		this.equipa = 0;
		this.jogador = 0;

		this.mousex = Input.mousePosition.x;
		this.mousey = Input.mousePosition.y;
		this.alpha = 0f;
		this.beta = 0f;
	}

	
	// Update is called once per frame
	void Update ()
	{
		if (gameObject.GetComponent<Camera> ().enabled) {
			if (Input.GetMouseButtonDown (0)) {
				int n_equipas = Jogo.equipas.Length;
				this.equipa = (this.equipa + 1) % n_equipas;
				this.jogador = 0;
				//print ("following: team " + this.equipa + " player " + this.jogador);
			}
			if (Input.GetMouseButtonDown (1)) {
				int n_jogadores = Jogo.equipas [0].get_jogadores ().Length;
				this.jogador = (this.jogador + 1) % n_jogadores;
				//print ("following: team " + this.equipa + " player " + this.jogador);
			}

			position_camera ();

			//meter texto
			info_player ();
		}
	}
 
}
