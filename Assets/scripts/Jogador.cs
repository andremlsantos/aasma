﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Jogador
{
	/***********************
	 * private variables
	 **********************/
	private int equipa;
	protected int id;
	protected int pontos; // score actually held by current object not by team

	private int raio_visao;

	private float stamina;
	protected float current_stamina;
	private static float stamina_ratio = 0.5f; // 50%
	private int peso;
	protected int current_peso;

	protected int imovable; // used when atacked so object cant act for a few turns
	private bool resting = false;

	private Material cor;
	private Material cor2 = (Material)Resources.Load ("Materials/White_loss_life");
	protected GameObject objeto;
	private float angle; // radianos
	private Vector3 direcao_olhar; // centered in origion

	private string ultima_acao = "";

	private bool undefined = false;


	// default position of all players based on teams
	private static Vector3[] position = {
		new Vector3 (5.0F, 0.0F, 250.0F),
		new Vector3 (495.0F, 0.0F, 250.0F),
		new Vector3 (250.0F, 0.0F, 5.0F),
		new Vector3 (250.0F, 0.0F, 495.0F)
	};
	// values for create players in different positions for each team
	private static Vector3[] adjust_position = {
		new Vector3 (5.0F, 0.0F, 0.0F),
		new Vector3 (-5.0F, 0.0F, 0.0F),
		new Vector3 (0.0F, 0.0F, 5.0F),
		new Vector3 (0.0F, 0.0F, -5.0F)
	};

	// value for everyone start to move forward
	private static float[] angle_default = {
		(Mathf.PI / 2f),
		(3f * Mathf.PI) / 2f,
		0,
		Mathf.PI
	};

	public Jogador (bool u)
	{
		this.undefined = u;
	}


	public Jogador (int equipa, int id, Material cor, float stamina, int raio_visao, int peso)
	{
		this.equipa = equipa;
		this.id = id;
		this.cor = cor;
		this.pontos = 0;
		this.raio_visao = raio_visao;
		this.stamina = stamina;
		this.current_stamina = stamina;
		this.peso = peso;
		this.current_peso = 0;
		this.imovable = 0;
		this.angle = Jogador.angle_default [equipa];
		draw ();

	}


	/*******************************************************************************************************
	 * private methods
	 *******************************************************************************************************/
	private bool draw ()
	{
		this.objeto = MonoBehaviour.Instantiate (Resources.Load ("prefabs/Robot")) as GameObject;
		this.objeto.transform.Translate (position [this.equipa]);

		// adjusting position for each player
		this.objeto.transform.Translate (new Vector3 (
				this.id * adjust_position [this.equipa].x,
				0,
				this.id * adjust_position [this.equipa].z
		));

		this.direcao_olhar = new Vector3 (Mathf.Sin (this.angle), 0f, Mathf.Cos (this.angle));
		this.objeto.transform.LookAt (this.direcao_olhar + this.objeto.transform.position);

		this.objeto.GetComponent<MeshRenderer> ().material = cor;

		Transform[] ts = this.objeto.GetComponentsInChildren<Transform> ();
		foreach (Transform child in ts) {
			child.gameObject.GetComponent<MeshRenderer> ().material = cor;
		}

		agentControl a = (agentControl)this.objeto.GetComponent (typeof(agentControl));
		a.id = this.id;
		a.equipa = this.equipa;

		return true;
	}

	private void print_vector3 (Vector3 a)
	{
		MonoBehaviour.print ("x: " + a.x + ", y: " + a.y + ", z: " + a.z);
	}


	/*
	 * function that remove hp from agent
	 */
	private void tirar_vida ()
	{
		this.current_stamina -= 0.02f;
		this.current_stamina = Mathf.Max (this.current_stamina, 0f);
	}

	/*
	 * control the agent 
	 * dont exit the terrain
	 */
	private void rodar_barreira_esquerda ()
	{
		this.objeto.transform.Rotate (0, 270, 0);
	}
	
	/*
	 * control the agent 
	 * dont exit the terrain
	 */
	private void rodar_barreira_direita ()
	{
		this.objeto.transform.Rotate (0, 90, 0);
	}
	
	/*
	 * control the agent 
	 * dont exit the terrain
	 */
	private void rodar_barreira_baixo ()
	{
		this.objeto.transform.Rotate (0, 180, 0);
	}
	
	/*
	 * control the agent 
	 * dont exit the terrain
	 */
	private void rodar_barreira_cima ()
	{
		this.objeto.transform.Rotate (0, 180, 0);
	}


	/************************************************************************************************************
	 * public methods
	 ************************************************************************************************************/
	public abstract string get_gui_text ();

	public bool is_undefined ()
	{
		return this.undefined;
	}

	public void update_geral ()
	{
		/*if (this.id == 1) {
			System.Threading.Thread.Sleep(10000);
		}*/

		this.objeto.transform.LookAt (this.direcao_olhar + this.objeto.transform.position);

		//meter bau em cima dele para dar ilusao de estar a carregar
		//indice 5 e o bau
		if (pontos > 0) {
			this.objeto.transform.GetChild (5).gameObject.SetActive (true);
		} else {
			this.objeto.transform.GetChild (5).gameObject.SetActive (false);
		}
	}

	public abstract void update ();


	public int get_equipa ()
	{
		return this.equipa;
	}

	public int get_id ()
	{
		return this.id;
	}

	public GameObject get_objeto ()
	{
		return this.objeto;
	}

	public int get_pontos ()
	{
		return this.pontos;
	}

	public int get_immovable ()
	{
		return this.imovable;
	}

	public string get_utlima_accao ()
	{
		return this.ultima_acao;
	}

	/**
	 * returns the current speed the current object moves
	 */
	public float get_current_speed ()
	{
		return Mathf.Max (
			(0.7f * (this.current_stamina / this.stamina) * Jogo.get_velocidade_base ()  //stamina component
			+
			0.3f * (((this.peso - current_peso) / this.peso) * Jogo.get_velocidade_base ())), // weight component
			0.1f * Jogo.get_velocidade_base () // minimum speed allowed
		);
	}
	
	public string get_tipo ()
	{
		return "jogador";
	}

	public int get_raio_visao ()
	{
		return this.raio_visao;
	}

	public bool is_immovable ()
	{
		if (this.imovable > 0)
			return true;
		return false;
	}
	
	public float distancia_2_objectos (Vector3 obj1, Vector3 obj2)
	{
		return Mathf.Sqrt (Mathf.Pow (obj1.x - obj2.x, 2) + Mathf.Pow (obj1.z - obj2.z, 2));
	}

	//TODO apagar uma destas
	public static float distancia_2_objectos2 (Vector3 obj1, Vector3 obj2)
	{
		return Mathf.Sqrt (Mathf.Pow (obj1.x - obj2.x, 2) + Mathf.Pow (obj1.z - obj2.z, 2));
	}

	
	/*
	 * checks if current object is in position posicao 
	 */
	public bool is_me (Vector3 posicao)
	{
		if (this.objeto.transform.position.x == posicao.x && this.objeto.transform.position.z == posicao.z) {
			return true;
		}
		return false;
	}

	/*
	 * this object gets hit
	 */
	public void gets_hit ()
	{
		//main.pausa = true;
		this.current_stamina -= 10.0f;
		this.imovable = 200;

		if (this.pontos > 0)
			this.largar_tesouro_chao ();
	}

	/*
	 * return current stamina from player
	 */
	public float get_current_stamina ()
	{
		return this.current_stamina;
	}

	/*
	 * return current weight from player
	 */
	public float get_peso ()
	{
		return this.current_peso;
	}

	/*********************************************************************************************************
	 *          sensors                                                                                      *
	 *********************************************************************************************************/
	/*
	 * Returns a list of treasure containers in sight for the current object
	 * Returns empty list when none is in sight
	 */
	public List<Container> Tesouro_vista ()
	{
		List<Container> posicao_tesouros = new List<Container> ();
		//posicao actual agente
		Vector3 actual_position = this.objeto.transform.position;

		foreach (Container c in Jogo.tesouros) {
			if (distancia_2_objectos (c.get_objeto ().transform.position, actual_position) < this.raio_visao)
				posicao_tesouros.Add (c);
		}
		return posicao_tesouros;
	}

	/*
	 * Checks if the current object can transport another treasure
	 * returns true if the object cannot transport other treasure, false otherwise
	 * Note: its considered here a player can only transport one treasure at the time
	 */
	public bool posso_carregar ()
	{
		return (this.pontos == 0);
	}

	/*
	 * returns if there are any treasures at my sight
	 */
	public bool is_tesouros_vista ()
	{
		List<Container> l = Tesouro_vista ();

		return l.Count != 0;
	}

	/*
	 * checks if there are treasures at my reach
	 */
	public bool is_tesouro_alcance ()
	{
		List<Container> l = tesouro_alcance ();
		return l.Count != 0;
	}

	/*
	 * catches the first treasure it can reach
	 */
	public void catch_primeiro_tesouro_alcance ()
	{
		List<Container> l = tesouro_alcance ();

		if (l.Count != 0) {
			apanhar_tesouro_alcance (l [0]);
		}
	}


	/*
	 * Checks if there is a treasure at the current object's reach
	 * returns one of the containers randomly if there is any, null otherwise
	 */ 
	public List<Container> tesouro_alcance ()
	{
		List<Container> posicao_tesouros = new List<Container> ();
		//posicao actual agente
		Vector3 actual_position = this.objeto.transform.position;

		foreach (Container c in Jogo.tesouros) {
			if (distancia_2_objectos (c.get_objeto ().transform.position, actual_position) < 1f)  
				posicao_tesouros.Add (c);
		}
		
		return posicao_tesouros;
	}

	/*
	 * Checkes if there is any teammates in sight carrying treasures and being chased by enemies
	 */
	public bool is_colega_aflito_vista ()
	{
		if (colega_aflito_vista ().Count > 0) {
			//main.pausa = true;
			return true;
		}

		return false;
	}

	/*
	 * Checks if there is any teammate being chased by an enemy
	 * returns a list of enemies chasing alies, emtpy list otherwise
	 * Note: returns a list of enemies not alies
	 */
	public List<Jogador> colega_aflito_vista ()
	{
		//int proximidade = raio_visao / 2; 
		//posicao actual agente
		Vector3 actual_position = this.objeto.transform.position;
		//jogador aflito
		List<Jogador> jogador_aflito = new List<Jogador> ();


		foreach (Jogador aliado in Jogo.equipas[this.equipa].get_jogadores()) {
			if (aliado.get_id () != this.id && distancia_2_objectos (aliado.get_objeto ().transform.position, actual_position) < this.raio_visao && aliado.get_pontos () > 0) {
				foreach (Jogador inimigo in this.inimigo_vista()) {
					if (inimigo.get_pontos () == 0 && distancia_2_objectos (aliado.get_objeto ().transform.position, inimigo.get_objeto ().transform.position) < raio_visao)
						jogador_aflito.Add (inimigo);
				}
			}
		}

		return jogador_aflito;
	}
	/*
	 * returns team mates with gold
	 */
	private List<Jogador> meus_colegas_tesouro (int equipa)
	{
		//jogadores minha equipa
		Jogador[] colegas = Jogo.equipas [this.equipa].get_jogadores ();
		List<Jogador> colegas_tesouro = null;
		foreach (Jogador colega in colegas) {
			//tenho tesouro se pontos forem maiores zero
			if (colega.pontos != 0) {
				colegas_tesouro.Add (colega);
			}
		}
		return colegas_tesouro;
	}

	public bool is_inimigo_alcance ()
	{
		List<Jogador> l = inimigo_alcance ();

		if (l.Count > 0) {
			return true;
		}
		return false;
	}


	/*
	 * Checks if there is any enemy at current object's reach
	 * returns a randomly selected enemy if any, null if there are none
	 */
	public List<Jogador> inimigo_alcance ()
	{
		//posicao actual agente
		Vector3 actual_position = this.objeto.transform.position;
		//obter objectos dentro raio visao
		//Collider[] hitColliders = Physics.OverlapSphere (actual_position, raio_visao);
		//lista inimigos que podem atacar
		List<Jogador> inimigo_alcance = new List<Jogador> ();

		foreach (Jogador inimigo in Jogo.equipas[this.equipa].get_inimigos()) {
			if (!inimigo.is_immovable () && distancia_2_objectos (inimigo.get_objeto ().transform.position, actual_position) < 3f)
				inimigo_alcance.Add (inimigo);
		}

		return inimigo_alcance;
	}

	/*
	 * checks if there are any enemies on sight carrying treasures
	 */
	public bool is_inimigo_tesouro_vista ()
	{
		if (inimigo_com_tesouro_vista ().Count > 0) {
			//print ("inimigo encontrado");
			return true;
		}

		return false;
	}

	/*
	 * Checks if there is any enemy on sight carrying a treasure
	 * returns a list of enemy players
	 */
	public List<Jogador> inimigo_com_tesouro_vista ()
	{
		//posicao actual agente
		Vector3 actual_position = this.objeto.transform.position;

		//lista inimigos que podem atacar
		List<Jogador> inimigo_com_tesouro_vista = new List<Jogador> ();

		foreach (Jogador inimigo in Jogo.equipas[this.equipa].get_inimigos()) {
			if (distancia_2_objectos (inimigo.get_objeto ().transform.position, actual_position) < this.raio_visao && inimigo.get_pontos () > 0)
				inimigo_com_tesouro_vista.Add (inimigo);
		}
		return inimigo_com_tesouro_vista;
	}

	/*
	 * moves to the first enemy carrying a treasure on sight
	 */
	public void interceptar_primeiro_inimigo_lista ()
	{
		List<Jogador> l = inimigo_com_tesouro_vista ();
		
		//main.pausa = true;
		
		if (l.Count > 0)
			interceptar_inimigo (l [0]);
		
		this.ultima_acao = "atacar";
	}


	/*
	 * Checks if there is any enemy on sight
	 * returns a list of enemy players
	 */
	public List<Jogador> inimigo_vista ()
	{
		//posicao actual agente
		Vector3 actual_position = this.objeto.transform.position;
		
		//lista inimigos que podem atacar
		List<Jogador> inimigo_vista = new List<Jogador> ();
		
		foreach (Jogador inimigo in Jogo.equipas[this.equipa].get_inimigos()) {
			if (!inimigo.is_immovable () && distancia_2_objectos (inimigo.get_objeto ().transform.position, actual_position) < this.raio_visao)
				inimigo_vista.Add (inimigo);
		}
		return inimigo_vista;
	}

	/*
	 * checks if the teams base can be seen
	 * returns true if it can false otherwise
	 */
	public bool base_vista ()
	{
		if (distancia_2_objectos (this.objeto.transform.position, Jogo.equipas [this.equipa].get_base ().get_position ()) <= this.raio_visao)
			return true;

		return false;
	}

	/*
	 * checks if current object is currently in his team's base
	 * returns true if it is, false otherwise
	 */
	public bool estou_base ()
	{
		return Jogo.equipas [this.equipa].get_base ().in_base (this.objeto.transform.position);
	}

	/**
	 * checks if the player can act being it by beins tired or attacked
	 */
	public bool can_act ()
	{
		if (this.imovable > 0 || this.resting) {
			//retirar unidades ao imovable para ele estar parado
			if (this.imovable > 0) {
				this.imovable--;
				//print ("estou parado " + this.equipa + " " + this.id);
				//enquanto isto acontece queremos dar ilusao de ele estar mal
				if (this.imovable % 20 < 10) {
					//mudar cor robo
					this.objeto.GetComponent<MeshRenderer> ().material = cor;
				} else {
					this.objeto.GetComponent<MeshRenderer> ().material = cor2;
				}
			}

			return false;
		}

		return true;
	}

	/*
	 *  checks if the current object is tired
	 *  returns true if it is, false otherwise
	 */
	public bool cansado ()
	{
		return ((this.current_stamina / this.stamina) <= Jogador.stamina_ratio);
	}

	/*
	 * returns the racio of weight used in this moment
	 */
	public float peso_racio ()
	{
		return (float)this.current_peso / (float)this.peso;
	}

	/*
	 * returns the racio of stamina remaining in this moment
	 */
	public float stamina_racio ()
	{
		return this.current_stamina / this.stamina;
	}

	/*
	 * checks id the agents has full stamina 
	 */
	public bool stamina_full ()
	{
		if (this.current_stamina >= this.stamina)
			return true;

		return false;
	}

	public bool estou_descansar ()
	{
		return this.resting;
	}

	/***********************************************************************************************************
	 *          effectors                                                                                      *
	 ***********************************************************************************************************/

	/*
	 * The current object holds the Container t
	 */
	public void apanhar_tesouro_alcance (Container t)
	{
		if (this.can_act ()) {
			if (!t.is_undefined ()) {
				this.pontos += t.get_value ();
				this.current_peso += t.get_peso ();

				int index = t.my_index ();

				if (index != -1) {
					Jogo.tesouros.RemoveAt (index);
					t.destroy ();
				}

				tirar_vida ();

				this.ultima_acao = "apanhar tesouro alcance";
			}
		}
	}

	/*
	 * Moves thouwards the first treasure it can see
	 */
	public void mover_primeiro_tesouro_vista ()
	{
		if (this.can_act ()) {
			List<Container> l = Tesouro_vista ();

			if (l.Count != 0) {
				mover_direcao_a (l [0].get_posicao ());
			}

			this.ultima_acao = "mover primeiro tesouro vista";
		}
	}

	/*
	 * The current object moves in direction of argument
	 */
	public void mover_direcao_a (Vector3 direcao)
	{
		if (this.can_act ()) {
			// translate for current object position
			Vector3 V = new Vector3 (
			direcao.x - this.objeto.transform.position.x,
			0,
			direcao.z - this.objeto.transform.position.z
			);

			// normalize input vector
			V.Normalize ();

			this.direcao_olhar = V;

			// scale it to the desired length
			V *= get_current_speed ();

			this.objeto.transform.position = new Vector3 (
			Mathf.Clamp (this.objeto.transform.position.x + V.x, 0f, 500f),
			this.objeto.transform.position.y,
			Mathf.Clamp (this.objeto.transform.position.z + V.z, 0f, 500f)
			);
			tirar_vida ();
		}
	}

	public void virar_direcao (Vector3 direcao)
	{
		if (this.can_act ()) {
			Vector3 aux = direcao.normalized;
			this.direcao_olhar = aux;
		}
	}

	/*
	 * The current object moves towards his team base if said base can be seen or radomply otherwise
	 */
	public void ir_direcao_base ()
	{
		if (this.can_act ()) {
			if (base_vista ()) {
				Vector3 V = Jogo.equipas [this.equipa].get_base ().get_position ();
				mover_direcao_a (V);
				this.ultima_acao = "ir direcao base";
			} else {
				mover_aleatorio ();
			}
		}
	}

	/**
	 * skips an action, if its tired rests
	 */
	public void skip_frame ()
	{
		if (this.resting) {
			this.current_stamina += 0.25f;
			// para de descansar
			if (this.current_stamina >= this.stamina) {
				this.resting = false;
			}
		}
		if (this.imovable > 0)
			this.imovable--;
		this.ultima_acao = "skip";
	}

	/*
	 * The current object takes no action and recovers a little stamina (?)
	 * Note: can only be used at team's base
	 */
	public void descansar ()
	{
		if (this.resting)
			this.skip_frame ();
		if (this.estou_base ()) {
			this.resting = true;
			this.ultima_acao = "descansar";
		}

	}

	/*
	 * Attacks the first enemy at reach
	 */
	public void atacar_primeiro_inimigo_alcance ()
	{
		if (this.can_act ()) {
			List<Jogador> l = inimigo_alcance ();

			if (l.Count > 0) {
				atacar_inimigo_alcance (l [0]);
			}
			this.ultima_acao = "atacar primeiro inimigo ao alcance";
		}
	}

	/*
	 * Attacks the enemy passed as parameter
	 * Note: can only attack an enemy who is nearby
	 */
	public void atacar_inimigo_alcance (Jogador inimigo)
	{
		if (this.can_act ()) {
			inimigo.gets_hit ();
		}
	}

	/*
	 * moves to the first enemy chasing an ally
	 */
	public void interceptar_primeiro_a_perseguir_amigo ()
	{
		if (this.can_act ()) {
			List<Jogador> l = colega_aflito_vista ();

			//main.pausa = true;

			if (l.Count > 0)
				interceptar_inimigo (l [0]);

			this.ultima_acao = "defender aliado interceptar";
		}
	}

	/*
	 * Moves towards the arguments position
	 */
	public void interceptar_inimigo (Jogador Inimigo)
	{
		if (this.can_act ()) {
			//print ("interceptar_inimigo " + this.equipa + " " + this.id);
			mover_direcao_a (Inimigo.get_objeto ().transform.position);
		}
	}

	/*
	 * Randomly rotates the current object
	 */
	public void girar_aleatorio ()
	{
		if (this.can_act ()) {
			this.angle += Random.Range (-Mathf.PI / 2f, Mathf.PI / 2f);
		}
	}


	/*
	 * drops a treasure on the base
	 * note: if the player is not currentlly on his team's base it does nothing
	 */
	public void largar_tesouro_base ()
	{
		if (this.can_act ()) {
			if (estou_base ()) {
				Jogo.equipas [this.equipa].get_base ().drop_tesouro (this.pontos);
				this.pontos = 0;
				this.current_peso = 0;
				this.ultima_acao = "largar tesouro base";
			}
		}
	}

	/*
	 * drops his current held treasure in the ground a little bit in front
	 */
	public void largar_tesouro_chao ()
	{
		//print ("entrou dropar");
		if (this.pontos > 0) {
			Container t = new Container ((this.objeto.transform.position + 3f * this.direcao_olhar), this.current_peso, this.pontos);

			Jogo.tesouros.Add (t);

			this.pontos = 0;
			this.current_peso = 0;
			this.ultima_acao = "largar tesouro chao";
		}
	}
	
	/*
	 * move agent towards its own vector
	 * based on current position 
	 * not angle
	 */
	public void mover_frente ()
	{
		if (this.can_act ()) {
			//radianos
			Vector3 V = new Vector3 (Mathf.Sin (this.angle), 0f, Mathf.Cos (this.angle));

			this.direcao_olhar = V;
		
			// scale it to the desired length
			V *= get_current_speed ();
		
			this.objeto.transform.position = new Vector3 (
			Mathf.Clamp (this.objeto.transform.position.x + V.x, 0f, 500f),
			this.objeto.transform.position.y,
			Mathf.Clamp (this.objeto.transform.position.z + V.z, 0f, 500f)
			);
			tirar_vida ();

			this.ultima_acao = "mover frente";
		}
	}


	/*
	 * Move agent based on two functions
	 * randomly or move fowards
	 */
	public void mover_aleatorio ()
	{
		if (this.can_act ()) {
			// rotates 1 in each 20 frames
			int max_random = Random.Range (0, 20);

			if (max_random == 0) 
				girar_aleatorio ();
			else
				mover_frente ();
		}
	}

	/*
	 * Contar tesouros apenas defenidos
	 */
	public int count_tesouros (List<Container> lista)
	{
		int count = 0;
		foreach (Container c in lista) {
			if (!c.is_undefined ()) {
				count++;
			}
		}
		return count;
	}
}