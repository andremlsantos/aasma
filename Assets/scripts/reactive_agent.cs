﻿using UnityEngine;
using System.Collections;
using System; 

public class reactive_agent : Jogador
{

	/**
	 * Uses constructor from super class
	 */
	public reactive_agent (int equipa, int id, Material cor, float stamina, int raio_visao, int peso) 
		: base(equipa, id, cor, stamina, raio_visao, peso)
	{
		//construtor jogadores
		//nao mecher
	}
	
	/***********************************************************************************
	 * Update - shoud be called since this script is not bounded with any game object
	 * *******************************************************************************/
	public override void update ()
	{
		double ini = 0;
		if (main.islogging) {
			ini = Time.realtimeSinceStartup;
		}
		this.update_geral ();
		ciclo ();

		if (main.islogging) {
			double end = Time.realtimeSinceStartup;
			main.LOG.addLogArray(new string[]{"reativo", this.get_equipa()+" "+this.get_id(), (end - ini).ToString()});
		}
	}

	/**
	 * Returns gui text for current agent type
	 */
	public override string get_gui_text()
	{
		return "Player: " + this.id + " - Reactive Agent \n" +
			"Points: " + this.pontos + 
				"\n" +
				"Stamina: " + Math.Round (this.current_stamina, 3) + "\n" +
				"Weight: " + this.current_peso + "\n" +
				"Immovable: "+ this.imovable +"\n" +
				"Last action: "+ this.get_utlima_accao() + "\n" +
				"Actual speed: "+this.get_current_speed();
	}

	public void ciclo()
	{
		// posso agir
		if (!this.can_act ())
			this.skip_frame ();

		//descansar
		else if (this.estou_base () && this.cansado ())
			this.descansar ();
		else if (this.base_vista() && this.cansado ())
			this.ir_direcao_base ();

		// levar tesouro para a base
		else if (!this.posso_carregar () && this.estou_base ())
			this.largar_tesouro_base ();
		else if (!this.posso_carregar ())
			this.ir_direcao_base ();

		//atacar inimigo
		else if (this.posso_carregar () && this.is_inimigo_alcance ())
			this.atacar_primeiro_inimigo_alcance ();

		// defender aliado
		else if (this.posso_carregar () && this.is_colega_aflito_vista ())
			this.interceptar_primeiro_a_perseguir_amigo ();

		// perseguir inimigo
		else if (this.posso_carregar() && this.is_inimigo_tesouro_vista ())
			this.interceptar_primeiro_inimigo_lista ();

		//procurar tesouros
		else if (this.is_tesouro_alcance () && this.posso_carregar ())
			this.catch_primeiro_tesouro_alcance ();
		else if (this.is_tesouros_vista ())
			this.mover_primeiro_tesouro_vista ();

		//default
		else
			this.mover_aleatorio ();
	}
}
