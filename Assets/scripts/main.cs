﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class main : MonoBehaviour
{
	/**
	 * Variables controling the game world
	 * change to taste
	 */
	/* number of teams in play min 2 max 4 */
	public int num_equipas = 2;
	/* number ot players in each team */
	public int num_jogadores = 1;
	/* base movement speed, used in all calculations */
	public float velocidade_base = 1.0f;
	/* number of treasures */
	public int tesouros = 1;
	/* stamina each player has */
	public float stamina = 100.0f;
	/* weight each player can transport */
	public int peso = 100;
	/* raio visao do agente */
	public int raio_visao = 10;
	/* game speed */
	public float game_speed = 0.0f;
	/* create log file NOTE: runing the project with this on may cause the program to run extremelly low */
	public bool log = false;
	/* log file name */
	public string logfile = "log.csv";

	/**
	 * Type of agent for each team
	 * r => reactive
	 * d => deliberative
	 * e => emotional
	 * h => hibrido
	 */
	public string tipo_equipa_0 = "r";
	public string tipo_equipa_1 = "r";
	public string tipo_equipa_2 = "r";
	public string tipo_equipa_3 = "r";

	/************************/

	/*********************
	 * cameras control
	 *******************/
	public Camera above;
	public Camera follow;
	public Camera free;
	public Camera free2;
	/********************/

	private float last_time = 0f;

	private Jogo jogo;
	public static bool pausa;
	public static bool islogging;
	public static Log LOG;

	/* PAUSA JOGO */
	int max_pontos;

	// Use this for initialization
	void Start ()
	{
		// creates array with agent types
		string [] types = {this.tipo_equipa_0, this.tipo_equipa_1, this.tipo_equipa_2, this.tipo_equipa_3};

		// creates and starts the new game
		jogo = new Jogo (num_equipas, num_jogadores, velocidade_base, tesouros, stamina, raio_visao, peso, types);
		main.pausa = true;

		// sets cameras
		above.enabled = true;
		follow.enabled = false;
		free.enabled = false;
		free2.enabled = false;

		//mostar texto pontuacao
		for (int i=0; i<num_equipas; i++) {
			//obter referencia texto
			GUIText texto = GameObject.Find ("pont_" + i).GetComponent<GUIText> ();
			//activar texto
			texto.enabled = true;
		}
		if (this.log) {
			main.islogging = true;
			main.LOG = new Log (this.logfile);
		}

		max_pontos = max_pontos_jogo ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		// mouse right click: cycle through cameras
		if (Input.GetKeyUp (KeyCode.Space)) {
			if (above.enabled) {
				above.enabled = false;
				follow.enabled = true;
				free.enabled = false;
				//free2.enabled = false;
				//activar texto 
				GUIText texto = GameObject.Find ("info_player").GetComponent<GUIText> ();
				texto.enabled = true;
			} else
		if (follow.enabled) {
				above.enabled = false;
				follow.enabled = false;
				free.enabled = true;
				//free2.enabled = false;
				//desactivar texto
				GUIText texto = GameObject.Find ("info_player").GetComponent<GUIText> ();
				texto.enabled = false;
			} else
		if (free.enabled) {
				above.enabled = true;
				follow.enabled = false;
				free.enabled = false;
				//free2.enabled = true;
			} /*else
		if (free2.enabled) {
				above.enabled = true;
				follow.enabled = false;
				free.enabled = false;
				free2.enabled = false;
			}*/
		}

		// pauses game
		if (Input.GetKeyUp (KeyCode.Return)) {
			main.pausa = !main.pausa;
		}

		// controls game speed
		if (Input.GetKeyUp (KeyCode.KeypadPlus)) {
			this.game_speed -= 0.01f;
			if (this.game_speed < 0f)
				this.game_speed = 0f;

			//print ("game_speed: " + this.game_speed);

		}
		if (Input.GetKeyUp (KeyCode.KeypadMinus)) {
			this.game_speed += 0.01f;
			//print ("game_speed: " + this.game_speed);
		}

		if ((Time.time - last_time) > game_speed) {
			// calls update routine for every player object
			if (!main.pausa) {
				this.jogo.update ();
			}

			last_time = Time.time;
		}

		int somatorio_pontos = somatorio_equipas ();
		if (somatorio_pontos >= max_pontos && !main.pausa) {
			//parar jogo
			main.pausa = true;
			double tempo = Time.realtimeSinceStartup;
			//MonoBehaviour.print ("Tempo total " + tempo);
		}
	}

	//maximo pontos num jogo
	private int max_pontos_jogo ()
	{
		int max_pontos = 0;
		List<Container> tesouros = Jogo.tesouros;

		foreach (Container c in tesouros) {
			max_pontos += c.get_value ();
		}
		return max_pontos;
	}

	//nas bases
	private int somatorio_equipas ()
	{
		int n = 0;
		foreach (Equipa e in Jogo.equipas) {
			n += e.get_base ().get_pontos ();
		}
		return n;
	}
}
