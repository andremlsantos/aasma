﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Jogo
{
	/***********************
	 * public variables
	 **********************/
	public static List<Container> tesouros;
	public static Equipa[] equipas;

	/***********************
	 * private variables
	 **********************/
	private static float velocidade_base;
	private int num_equipas;
	private int num_jogadores;
	private int raio_visao;

	/*****************
	 * consrtructors
	 ******************/
	public Jogo (int num_equipas, int num_jogadores, float velocidade_base, int tesouros, float stamina, int raio_visao, int peso, string[] types)
	{
		this.num_equipas = num_equipas;
		this.num_jogadores = num_jogadores;
		Jogo.velocidade_base = velocidade_base;
		this.raio_visao = raio_visao;
		set_tesouros (tesouros);
		set_equipas (stamina, peso, types);

		// here because all the players are set
		set_inimigos ();

		//MonoBehaviour.print ("n tesouros: " + Jogo.tesouros.Count);
	}

	/***********************************************************************************
	 * Update - shoud be called since this script is not bounded with any game object
	 * *******************************************************************************/

	public void update ()
	{
		/*foreach (Equipa equipa in Jogo.equipas) {
			equipa.update ();
		}*/
	}

	/***********************
	 * private methods
	 *******************/
	/**
	 * Initializes the game with the number of teams selected
	 **/
	private bool set_equipas (float stamina, int peso, string[] types)
	{
		//array equipas
		Jogo.equipas = new Equipa[this.num_equipas];

		for (int i = 0; i < this.num_equipas; i++) {
			Jogo.equipas [i] = new Equipa (i, this.num_jogadores, stamina, raio_visao, peso, types [i]);
		}

		return true;
	}

	private void set_inimigos ()
	{
		for (int i = 0; i < this.num_equipas; i++) {
			Jogo.equipas [i].set_inimigos ();
		}
	}

	/*
	 * Initializes the containers randomly
	 */
	private void set_tesouros (int n)
	{
		Jogo.tesouros = new List<Container> ();

		for (int i = 0; i < n; i++) {
			Container aux = new Container ();
			Jogo.tesouros.Add (aux);
		}

	}

	/***********************
	 * public methods
	 *******************/
	public static float get_velocidade_base ()
	{
		return Jogo.velocidade_base;
	}


}