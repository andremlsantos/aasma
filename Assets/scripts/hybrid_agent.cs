﻿using UnityEngine;
using System.Collections;
using System; 

public class hybrid_agent : deliberative_agent
{

	private string camada;

	/**
	 * Uses constructor from super class
	 */
	public hybrid_agent (int equipa, int id, Material cor, float stamina, int raio_visao, int peso) 
		: base(equipa, id, cor, stamina, raio_visao, peso)
	{
		this.camada = "";
	}


	// Update is called once per frame
	public override void update ()
	{
		double ini = 0;
		if (main.islogging) {
			ini = Time.realtimeSinceStartup;
		}

		// posso agir
		if (!this.can_act ()) {
			this.camada = "Reativo";
			this.skip_frame ();
		}

		//atacar inimigo
		else if (this.posso_carregar () && this.is_inimigo_alcance ()) {
			this.camada = "Reativo";
			this.atacar_primeiro_inimigo_alcance ();
		}
		
		// defender aliado
		else if (this.posso_carregar () && this.is_colega_aflito_vista ()) {
			this.camada = "Reativo";
			this.interceptar_primeiro_a_perseguir_amigo ();
		}
		
		// perseguir inimigo
		else if (this.posso_carregar () && this.is_inimigo_tesouro_vista ()) {
			this.camada = "Reativo";
			this.interceptar_primeiro_inimigo_lista ();
		} else {
			this.camada = "Deliberativo";
			// agente deliberativo
			base.update ();
		}

		if (this.camada == "Reativo")
			this.update_geral ();

		if (main.islogging) {
			double end = Time.realtimeSinceStartup;
			main.LOG.addLogArray (new string[]{"hibrido", this.get_equipa () + " " + this.get_id (), (end - ini).ToString ()});
		}
	}
	/*******************************************************************************************************
	 * private methods
	 *******************************************************************************************************/


	/************************************************************************************************************
	 * public methods
	 ************************************************************************************************************/

	/**
	 * Show in interface
	 */
	public override string get_gui_text ()
	{
		return "Player: " + this.id + " - Hybrid Agent \n" +
			"Position: " + this.objeto.transform.position.x + " " + this.objeto.transform.position.z + "\n" +
			"Points: " + this.pontos + 
			"\n" +
			"Stamina: " + Math.Round (this.current_stamina, 3) + "\n" +
			"Weight: " + this.current_peso + "\n" +
			"Immovable: " + this.imovable + "\n" +
			"Last Action: " + this.get_utlima_accao () + "\n" +
			"Actual Speed: " + this.get_current_speed () + "\n" +
			"Layer: " + this.camada + "\n" +
			"Desire: " + this.desejo + "\n" +
			"Intection: " + this.intencao_to_string () + "\n" +
			"Plane: " + this.plano_to_string ();
	}
}
