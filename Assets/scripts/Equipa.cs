﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Equipa
{
	/***********************
	 * private variables
	 **********************/
	private int num_jogadores;
	private Jogador[] jogadores;
	private Base _base_; //base is a reserved word
	private int id;
	private List<Jogador> inimigos;

	private static Material[] cor = {
		(Material)Resources.Load ("Materials/Blue_team_opaque"), 
		(Material)Resources.Load ("Materials/Red_team_opaque"), 
		(Material)Resources.Load ("Materials/Green_team_opaque"), 
		(Material)Resources.Load ("Materials/Yellow_team_opaque")
	};


	/*****************
	 * consrtructors
	 ******************/
	public Equipa (int id, int num_jogadores, float stamina, int raio_visao, int peso, string type)
	{
		this.id = id;
		this.num_jogadores = num_jogadores;
		this._base_ = new Base (this.id, cor [this.id]);
		set_jogadores (stamina, raio_visao, peso, type);
	}

	/***********************************************************************************
	 * Update - shoud be called since this script is not bounded with any game object
	 * *******************************************************************************/
	
	public void update ()
	{
		foreach (Jogador jogador in this.jogadores) {
			jogador.update ();
		}
	}


	/*******************
	 * private methods
	 *******************/
	/*
	 * Initializaes the team
	 */
	private bool set_jogadores (float stamina, int raio_visao, int peso, string type)
	{
		//array jogadores
		this.jogadores = new Jogador[this.num_jogadores];

		for (int i = 0; i < this.num_jogadores; i++) {
			switch (type) {
			case "r":
				this.jogadores [i] = new reactive_agent (this.id, i, cor [this.id], stamina, raio_visao, peso);
				break;
			case "d":
				this.jogadores [i] = new deliberative_agent (this.id, i, cor [this.id], stamina, raio_visao, peso);
				break;
			case "h":
				this.jogadores [i] = new hybrid_agent (this.id, i, cor [this.id], stamina, raio_visao, peso);
				break;
			}
		}

		return true;
	}

	/*******************
	 * public methods
	 *******************/
	public Base get_base ()
	{
		return this._base_;
	}

	/*
	 * returns team color
	 */
	public Material get_color ()
	{
		return cor [this.id];
	}

	public Jogador[] get_jogadores ()
	{
		return this.jogadores;
	}

	public List<Jogador> get_inimigos ()
	{
		return this.inimigos;
	}

	public int get_num_jogadores ()
	{
		return this.num_jogadores;
	}

	public void set_inimigos ()
	{
		//ober numero equipas
		int n_equipas = Jogo.equipas.Length;
		//lista inimigos
		this.inimigos = new List<Jogador> ();

		for (int i=0; i<n_equipas; i++) {
			//se nao for da minha equipa e inimigo
			if (i != this.id) {
				Jogador[] temp_inimigos = Jogo.equipas [i].get_jogadores ();
				for (int j=0; j<temp_inimigos.Length; j++) {
					this.inimigos.Add (temp_inimigos [j]);
				}
			}
		}
	}
}
