﻿using UnityEngine;
using System.Collections;

public class Container
{
	private GameObject objeto;
	private int valor;
	private int peso;
	private Vector3 posicao;
	private bool undefined = false;

	/*****************
	 * consrtructors
	 ******************/
	// initializes at a random position
	public Container ()
	{
		set_position ();
		set_values ();
		draw ();
	}

	// null treasure
	public Container (bool u)
	{
		this.undefined = true;
	}

	public Container (int nada)
	{

	}

	// initializes at position p
	public Container (Vector3 p)
	{
		this.posicao = p;
		set_values ();
		draw ();
	}

	// initializes at position p, with value and weights pre defined
	public Container (Vector3 p, int peso, int valor)
	{
		// creates only inside the map
		if (p.x > 500)
			p.x = 500;
		if (p.x < 0)
			p.x = 0;
		if (p.z > 500)
			p.z = 500;
		if (p.z < 0)
			p.z = 0;

		this.posicao = p;
		this.peso = peso;
		this.valor = valor;
		draw ();
	}

	/***********************
	 * private methods
	 *******************/
	/**
	 * generates a random position for the container
	 */
	private void set_position ()
	{
		float x = (float)Random.Range (100, 401);
		float z = (float)Random.Range (100, 401);

		this.posicao = new Vector3 (x, 0.0f, z);
	}

	/*
	 * generates the value and weight for the container
	 */
	private void set_values ()
	{
		int aux = (int) Random.Range (10, 50);

		this.peso = aux;
		this.valor = (int)(peso * peso) / 2;

	}

	private bool draw ()
	{
		this.objeto = MonoBehaviour.Instantiate (Resources.Load ("prefabs/bau_tesouro")) as GameObject;
		this.objeto.transform.Translate (this.posicao);
		return true;
	}

	/***********************
	 * public methods
	 *******************/
	public int get_value ()
	{
		return this.valor;
	}

	public int get_peso ()
	{
		return this.peso;
	}

	public Vector3 get_posicao ()
	{
		return posicao;
	}

	public string get_tipo ()
	{
		return "tesouro";
	}

	public GameObject get_objeto ()
	{
		return this.objeto;
	}

	public void destroy ()
	{
		MonoBehaviour.Destroy (this.objeto);
	}

	public bool is_undefined ()
	{
		return this.undefined;
	}

	/*
	 * checks if current object is in position posicao 
	 */
	public bool is_me (Vector3 posicao)
	{
		if (this.objeto.transform.position.x == posicao.x && this.objeto.transform.position.z == posicao.z) {
			return true;
		}
		return false;
	}

	/*
	 * returns this object's index at containers list or -1 in case it doesnt find it
	 */
	public int my_index ()
	{

		for (int i=0; i<Jogo.tesouros.Count; i++) {
			if (Jogo.tesouros [i].is_me (this.posicao))
				return i;
		}
		return -1;
	}

	public static Container get_tesouro_coordenadas (float x, float y)
	{
		Vector3 v = new Vector3 (x, 0f, y);
		foreach (Container c in Jogo.tesouros) {

			if (c.is_me (v))
				return c;
		}
		return new Container (true);
	}

	/*
	 * check if current treasure is on the map
	 */
	public bool is_alive ()
	{
		return Jogo.tesouros.Contains (this);
	}
}
