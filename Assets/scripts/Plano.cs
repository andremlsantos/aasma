using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Plano
{
	//referencia a jogador
	private Jogador agente;
	private Intencao intencao;

	//referencia Plano
	List<string> plano;

	public Plano ()
	{
		this.plano = new List<string> ();
	}


	public Plano (Intencao i, Jogador j)
	{
		this.plano = new List<string> ();
		this.intencao = i;
		this.agente = j;
		//this.agente = agente;
		this.constroi_plano_com_base_numa_intencao ();
	}

	/*******************************************************************************************************
	 * private methods
	 *******************************************************************************************************/
	
	/*
	 * Constroi um plano para atingir uma determinada intencao
	 */
	private void constroi_plano_com_base_numa_intencao ()
	{
		switch (this.intencao.get_desejo ()) {
		case "procurar-tesouro":
			this.plano = new List<string> ();
			this.plano.Add ("mover_aleatorio");
			break;
		case "apanhar-tesouro":
			this.plano = new List<string> ();
			this.plano.Add ("rodar_direcao " + this.intencao.get_cordenadas_objectivo ().x + " " + this.intencao.get_cordenadas_objectivo ().z);
			this.plano.Add ("mover_frente " + this.intencao.get_cordenadas_objectivo ().x + " " + this.intencao.get_cordenadas_objectivo ().z);
			this.plano.Add ("apanhar_tesouro " + this.intencao.get_cordenadas_objectivo ().x + " " + this.intencao.get_cordenadas_objectivo ().z);
			break;
		case "largar-tesouro-base":
			this.plano = new List<string> ();
			this.plano.Add ("rodar_direcao " + this.intencao.get_cordenadas_objectivo ().x + " " + this.intencao.get_cordenadas_objectivo ().z);
			this.plano.Add ("mover_frente " + this.intencao.get_cordenadas_objectivo ().x + " " + this.intencao.get_cordenadas_objectivo ().z);
			this.plano.Add ("largar_tesouro");
			break;
		case "atacar-inimigo":
			//main.pausa = true;
			this.plano = new List<string> ();
			//this.plano.Add ("rodar_direcao " + this.intencao.get_cordenadas_objectivo ().x + " " + this.intencao.get_cordenadas_objectivo ().z);
			this.plano.Add ("perseguir-inimigo " + this.intencao.get_cordenadas_objectivo ().x + " " + this.intencao.get_cordenadas_objectivo ().z);
			this.plano.Add ("atacar-inimigo");
			break;
		case "descancar":
			this.plano = new List<string> ();
			this.plano.Add ("mover_frente " + this.intencao.get_cordenadas_objectivo ().x + " " + this.intencao.get_cordenadas_objectivo ().z);
			this.plano.Add ("descansar");
			this.plano.Add ("recuperar_stamina");
			break;
		case "defender-colega":
			this.plano = new List<string> ();
			this.plano.Add ("perseguir-inimigo " + this.intencao.get_cordenadas_objectivo ().x + " " + this.intencao.get_cordenadas_objectivo ().z);
			this.plano.Add ("atacar-inimigo");
			//main.pausa = true;
			break;
		case "escolta":
			this.plano = new List<string> ();

			this.plano.Add ("ir_proximo_aliado " + this.intencao.get_cordenadas_objectivo ().x + " " + this.intencao.get_cordenadas_objectivo ().z);
			this.plano.Add ("escoltar_aliado");
			//main.pausa = true;
			break;
		case "cooperar-tesouro":
			this.plano = new List<string> ();
			this.plano.Add ("larguar-tesouro-chao");
			this.plano.Add ("mover_frente " + this.intencao.get_cordenadas_objectivo ().x + " " + this.intencao.get_cordenadas_objectivo ().z);
			break;
		}



	}

	/************************************************************************************************************
	 * public methods
	 ************************************************************************************************************/

	public Intencao get_intencao ()
	{
		return this.intencao; 
	}

	public bool is_vazio ()
	{
		return this.plano.Count < 1;
	}

	public string to_string ()
	{
		string s = "[";

		foreach (string aux in this.plano) {
			s += aux + "; ";
		}
		s += "]";

		return s;
	}

	public void executa_acao (/*Container tesouro, Jogador inimigo*/)
	{
		char[] aux = {' '};
		float dist;

		if (this.plano.Count > 0) {
			string[] instrucao = this.plano [0].Split (aux);

			Vector3 v;

			switch (instrucao [0]) {
			case "mover_aleatorio":
				this.agente.mover_aleatorio ();
				this.plano.RemoveAt (0);
				break;
			case "rodar_direcao":
				v = new Vector3 (float.Parse (instrucao [1]), 0f, float.Parse (instrucao [2]));
				this.agente.virar_direcao (v);
				this.plano.RemoveAt (0);
				break;
			case "mover_frente":
				v = new Vector3 (float.Parse (instrucao [1]), 0f, float.Parse (instrucao [2]));
				this.agente.mover_direcao_a (v);
				if (this.agente.distancia_2_objectos (this.agente.get_objeto ().transform.position, v) <= 1.0f)
					this.plano.RemoveAt (0);
				break;
			case "apanhar_tesouro":
				//this.agente.apanhar_tesouro_alcance (Container.get_tesouro_coordenadas (float.Parse (instrucao [1]), float.Parse (instrucao [2])));
				this.agente.catch_primeiro_tesouro_alcance ();
				//this.agente.apanhar_tesouro_alcance (tesouro);
			//main.pausa = true;
				this.plano.RemoveAt (0);
				break;
			case "largar_tesouro":
				this.agente.largar_tesouro_base ();
				//main.pausa = true;
				this.plano.RemoveAt (0);
				break;

			case "perseguir-inimigo":
				v = new Vector3 (float.Parse (instrucao [1]), 0f, float.Parse (instrucao [2]));
				this.agente.mover_direcao_a (v);
				dist = this.agente.distancia_2_objectos (this.agente.get_objeto ().transform.position, v);
				//MonoBehaviour.print (bla);
				if (dist <= 0.2f) {
					this.plano.RemoveAt (0);
					this.executa_acao ();
				}
				break;		
			case "atacar-inimigo":
				this.agente.atacar_primeiro_inimigo_alcance ();
				//main.pausa = true;
				this.plano.RemoveAt (0);
				break;
			case "descansar":
				this.agente.largar_tesouro_base ();
				this.agente.descansar ();
				this.plano.RemoveAt (0);
				//main.pausa = true;
				break;
			case "recuperar_stamina":
				this.agente.skip_frame ();
				if (this.agente.stamina_full ()) {
					this.plano.RemoveAt (0);
				}
				break;
			case "ir_proximo_aliado":
				v = new Vector3 (float.Parse (instrucao [1]), 0f, float.Parse (instrucao [2]));
				dist = this.agente.distancia_2_objectos (this.agente.get_objeto ().transform.position, v);
				if (dist <= 15f) {
					this.plano.RemoveAt (0);
					this.executa_acao ();
				} else {
					this.agente.mover_direcao_a (v);
				}
				break;
			case "escoltar_aliado":
				this.agente.mover_aleatorio ();
				break;
			case "larguar-tesouro-chao":
				this.agente.largar_tesouro_chao ();
				break;
			}
		}
	}

}