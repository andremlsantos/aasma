using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Mensagem
{
	private string tipo;
	private List<Jogador> inimigo;
	private List<Container> tesouro;
	private Base _base;
	private deliberative_agent emissor;
	private bool need_escolta;

	//comunicar inimigos
	public Mensagem (string tipo, deliberative_agent emissor, List<Jogador> inimigo)
	{
		this.tipo = tipo;
		this.inimigo = inimigo;
		this.emissor = emissor;
	}

	//comunicar pedidos ajuda
	public Mensagem (string tipo, deliberative_agent emissor)
	{
		this.tipo = tipo;
		this.emissor = emissor;
	}

	//comunicar pedidos escolta
	public Mensagem (string tipo, deliberative_agent emissor, bool escolta)
	{
		this.tipo = tipo;
		this.emissor = emissor;
		this.need_escolta = escolta;
	}

	//comunicar tesouros visualizados
	public Mensagem (string tipo, deliberative_agent emissor, List<Container> tesouro)
	{
		this.tipo = tipo;
		this.tesouro = tesouro;
		this.emissor = emissor;
	}

	//comunicar base encontrada
	public Mensagem (string tipo, deliberative_agent emissor, Base _base)
	{
		this.tipo = tipo;
		this._base = _base;
		this.emissor = emissor;
	}

	public string get_tipo ()
	{
		return this.tipo;
	}

	public List<Jogador> get_inimigos ()
	{
		return this.inimigo;
	}

	public List<Container> get_tesouros ()
	{
		return this.tesouro;
	}

	public Base get_base ()
	{
		return this._base;
	}

	/*
	 * obtem jogadores equipa
	 */
	public void envia_mensagem ()
	{
		//obter colegas da minha equipa
		Jogador[] equipa = Jogo.equipas [emissor.get_equipa ()].get_jogadores ();
		//nao quero enviar mensagem a mim proprio
		foreach (deliberative_agent jogador in equipa) {
			if (jogador.get_id () != this.emissor.get_id ()) {
				//enviar dados
				receber_mensagem (jogador, this);
			}
		}
	}

	/*
	 * para fazer set dados
	 */
	public void receber_mensagem (deliberative_agent jogador, Mensagem mensagem)
	{
		//identificar tipo mensagem
		if (mensagem.get_tipo () == "base") {
			//conheco base
			jogador.set_know_base (true);
			//fazer set base
			jogador.set_base_ (this._base);
		} else if (mensagem.get_tipo () == "tesouros") {
			//envio mensagem de novos tesouros encontrados
			jogador.set_tesouros (this.tesouro);
		} else if (mensagem.get_tipo () == "remover-tesouros") {
			//ve tesoros que nao estao no mapa e remove da lista comunicacao
			jogador.remove_tesouros (this.tesouro);
		} else if (mensagem.get_tipo () == "apanhar-tesouro") {
			//se vou apanhar removo
			jogador.remove_tesouro (this.tesouro);
		} else if (mensagem.get_tipo () == "escolta") {
			jogador.set_escolta (this.emissor, this.need_escolta);
		} else if (mensagem.get_tipo () == "inimigos_tesouro") {
			jogador.set_inimigos_tesouro (this.emissor, this.inimigo);
		} else if (mensagem.get_tipo () == "coordenar-tesouro") {

		}
	}
}
