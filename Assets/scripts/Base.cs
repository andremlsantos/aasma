﻿using UnityEngine;
using System.Collections;

public class Base
{
	private GameObject objeto;
	private int pontos;
	private int equipa;
	private Material cor;

	// default position of all bases used in each bas construction
	private static Vector3[] position = {
		new Vector3 (0.0F, 0.0F, 250.0F),
		new Vector3 (500.0F, 0.0F, 250.0F),
		new Vector3 (250.0F, 0.0F, 0.0F),
		new Vector3 (250.0F, 0.0F, 500.0F)
	};

	// team names for printing
	private static string[] nome = {
		"Blue",
		"Red",
		"Green",
		"Yellow"
	};

	/*****************
	 * consrtructors
	 ******************/
	public Base (int equipa, Material cor)
	{
		this.equipa = equipa;
		this.cor = cor;
		this.pontos = 0;
		draw ();
	}


	/***********************
	 * private methods
	 *******************/
	private bool draw ()
	{
		this.objeto = MonoBehaviour.Instantiate (Resources.Load ("prefabs/base")) as GameObject;
		this.objeto.transform.Translate (position [this.equipa]);
		this.objeto.GetComponent<MeshRenderer> ().material = cor;

		return true;
	}


	/***********************
	 * public methods
	 *******************/
	public Vector3 get_position ()
	{
		return this.objeto.transform.position;
	}

	public GameObject get_objeto ()
	{
		return this.objeto;
	}

	public int get_pontos ()
	{
		return this.pontos;
	}

	/*
	 * check if the vector passed as argument is in base
	 */
	public bool in_base (Vector3 posicao)
	{
		float xmin = this.objeto.transform.position.x - 5;
		float xmax = this.objeto.transform.position.x + 5;
		float zmin = this.objeto.transform.position.z - 5;
		float zmax = this.objeto.transform.position.z + 5;

		if (posicao.x > xmin && posicao.x < xmax && posicao.z > zmin && posicao.z < zmax)
			return true;
		return false;
	}

	public static string[] get_cor_string ()
	{
		return (Base.nome);
	}

	/*
	 * receives a treasure incements its score
	 */
	public void drop_tesouro (int value)
	{
		this.pontos += value;
		//print (Base.nome [this.equipa] + ": " + this.pontos);
		GUIText texto = GameObject.Find ("pont_" + this.equipa).GetComponent<GUIText> ();
		if (this.equipa == 0) {
			texto.text = "Hybrid Team: " + this.pontos;
		} else if (this.equipa == 1) {
			texto.text = "BDI Team: " + this.pontos;
		} else if (this.equipa == 2) {
			texto.text = "Reactive Team: " + this.pontos;
		} else if (this.equipa == 3) {
			texto.text = "Equipa Amarela: " + this.pontos;
		} 
	}

}
