﻿using UnityEngine;
using System.Collections;
using System.IO;

public class Log{
	private StreamWriter file;
	private string name;

	public Log(string name)
	{
		bool exists = Directory.Exists("logs");
		
		if(!exists)
			System.IO.Directory.CreateDirectory("logs");

		this.name = name;
	}

	public void addLog(string log)
	{
		this.file = new StreamWriter ("logs/"+this.name, true);
		this.file.WriteLine (log);
		this.file.Close();
	}

	public void addLogArray(string[] logs)
	{
		this.file = new StreamWriter ("logs/"+this.name, true);
		for (int i=0; i<logs.Length; i++) {
			if(i != 0)
				this.file.Write(",");
			this.file.Write (logs [i]);
		}
		this.file.WriteLine ();
		this.file.Close();
	}
}
