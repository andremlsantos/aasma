using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Intencao
{
	/***********************
	 * private variables
	 **********************/
	private string desejo;
	private Vector3 cordenadas_objectivo ;
	//private Jogador inimigo;

	//para tesouros que sao estaticos
	public Intencao (string desejo, GameObject obj)
	{
		if (obj != null) {
			this.desejo = desejo;
			this.cordenadas_objectivo = obj.transform.position;
		}
	}

	//para inimigos que sao dinamicos
	public Intencao (string desejo, Jogador inimigo)
	{
		this.desejo = desejo;
		this.cordenadas_objectivo = inimigo.get_objeto ().transform.position;
		//this.inimigo = inimigo;
	}

	public Intencao (string desejo)
	{
		this.desejo = desejo;
	}

	public string to_string ()
	{
		return this.desejo + this.cordenadas_objectivo.x + " " + this.cordenadas_objectivo.z;
	}

	/*
	 * retorna desejo
	 */
	public string get_desejo ()
	{
		return this.desejo;
	}
	/*
	 * retorna cordenadas do objectivo
	 */
	public Vector3 get_cordenadas_objectivo ()
	{
		return this.cordenadas_objectivo;
	}

	//public Jogador get_inimigo ()
	//{
	//	return inimigo;
	//}

	public bool is_equal (Intencao i)
	{
		return (this.desejo == i.desejo && this.cordenadas_objectivo.x == i.get_cordenadas_objectivo ().x && this.cordenadas_objectivo.z == i.get_cordenadas_objectivo ().z);
	}

}
